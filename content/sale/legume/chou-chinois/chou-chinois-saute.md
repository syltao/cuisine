+++
title = "Chou Chinois Sauté"
date =  2021-02-21T19:16:01-03:00
weight = 5
+++
## Ingrédients (4 personnes) {#ingrédients--4-personnes}

-   1 chou chinois
-   2 oignons frais
-   1 gousse Ail
-   3 cm racine de gingembre
-   2 cuil. à soupe d'huile de sésame
-   2 cuil. à soupe Sauce soja
-   2 cuil. à soupe Grains de sésame


## Préparation {#préparation}

1.  Rincez le chou et coupez-le en morceaux. Pelez et émincez les oignons et l’ail. Pelez et râpez le morceau de gingembre.
2.  Chauffez l’huile dans un wok. Faites revenir les oignons, l’ail et le gingembre 2 min.
3.  Ajoutez le chou et faites revenir 5 min en mélangeant. Ajoutez la sauce soja et le sésame. Faites revenir le tout encore 2 min. Servez sans attendre


## Observations {#observations}

La taille des choux chinois peut beaucoup varier. Pour 4 personnes il faut une pièce entière de 20-25cm.
