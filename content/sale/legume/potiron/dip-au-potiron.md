+++
title = "Dip au Potiron"
date =  2021-02-21T19:16:54-03:00
weight = 5
+++
## Ingrédients (pour un grand bol) {#ingrédients--pour-un-grand-bol}

Réfrigération nécessaire

-   600g de potiron/citrouille
-   1 gousse d'ail
-   1/2 orange
-   1/2 citron
-   1CS d'huile d'olive
-   1CS de pâte de sésame
-   sel et poivre


## Préparation {#préparation}

1.  (Pelez) le potiron puis coupez le en dés. Pelez et émincez la gousse d'ail. Cuire préférentiellement à la vapeur.
2.  Mixer le potiron avec le jus d'une demi-orange et d'un demi-citron. Rajouter l'huile d'olive et la pâte de sésame, salez, poivrez et mixez de nouveau.
3.  Versez dans un bol et placez au réfrigérateur pendant au moins 2 heures. Servez à l'apéritif sur des toasts.


## Observation {#observation}

Se raffermit au froid. Pour le palais grossiers, un peu de piment ou de poivre relève le goût. Éviter les supports au goût trop prononcé.
