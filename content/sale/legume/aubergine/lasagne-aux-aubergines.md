+++
title = "Lasagne Aux Aubergines"
date =  2021-02-21T18:47:44-03:00
weight = 5
+++

## Ingrédients (6 personnes): {#ingrédients--6-personnes}

-   3 aubergines
-   6 gros champignons de Paris coupés en lamelles
-   Plaques de pâte à lasagnes (toutes prêtes ou faites maison)


### Sauce blanche : {#sauce-blanche}

-   2 cuil à soupe de farine
-   2 cuil à soupe de beurre (ou de margarine pour cuisson)
-   1/2 litre de lait (ou bouillon de légumes)
-   Sel, poivre


### Sauce tomatée : {#sauce-tomatée}

-   5 tomates
-   Basilic
-   Persil
-   2 gousses d'ail
-   2 oignons
-   sel, poivre, épices au choix


## Préparation : {#préparation}

1.  Laver et couper en lamelles les champignons
2.  Laver les aubergines et les couper en lamelles dans le sens de la longueur. Éventuellement les mettre dans une passoire, les saler et les laisser macérer 30 minutes. Pendant ce temps, faire la **sauce tomate**.
3.  Peler les oignons et l'ail, couper en dés. Faire revenir dans une poêle avec un peu d'huile d'olive.
4.  Hacher le persil et le basilic et ajouter dans la poêle.
5.  Couper les tomates (préalablement pelées à l'eau chaude) en dés et ajouter à la préparation. Saler, poivrer et épicer à votre goût.
6.  Faire mijoter une quinzaine de minutes en mélangeant régulièrement. Pendant que cela mijote, préparer la **sauce blanche**.
7.  Dans une casserole faire fondre le beurre (ou margarine) et ajouter en pluie la farine dans le beurre (ou margarine) fondu. Bien faire détacher de la casserole tout en mélangeant et ajouter peu à peu le lait (ou bouillon de légumes). Si vous utilisez le bouillon, il faut aller tout doucement.
       Bien mélanger et laisser cuire en remuant sans arrêt à feu très doux jusqu'à épaississement. Saler, poiver. La sauce doit être onctueuse mais un peu liquide.
8.  Rincer les aubergines à l'eau froide et faire revenir dans de l'huile d'olive jusqu'à ce qu'elles soient dorées et cuites.
9.  À ce stade il y a 4 éléments : sauces tomate, sauce blanche, aubergines, champignons. Dans un plat allant au four:
    -   étaler un peu de sauce tomate dans le fond. Puis disposer en couches successives les ingrédients :
    -   plaque de lasagne,
    -   aubergines 50%,
    -   champignons 50%,
    -   sauce blanche 66%,
    -   plaques de lasagnes,
    -   aubergines 50%,
    -   champignons 50%,
    -   sauce tomate 100%.
    -   Terminer par une couche de plaques de lasagnes et napper de sauce blanche restante
10. Faire cuire à four moyen environ 45 minutes.


## Observations {#observations}

C'est long, il faut beaucoup d'ustensiles. Mais c'est pas mal. Fromage râpé possible.
