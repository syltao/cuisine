+++
title = "Falafels"
date =  2021-02-21T19:01:01-03:00
weight = 5
+++

![falafels](../falafels.jpeg)

**Temps de préparation** : 25 minutes 
**Temps de repos :** 24 heures 
**Temps de cuisson :** 20 minutes 
**Quantité :** 3-4 personnes

#### Ingrédients
- 170 grammes de pois chiches secs (surtout pas en boîte)
- 2 gousses d'ail
- 15 grammes de persil plat
- 15 grammes de coriandre
- 1/4 d'oignon
- 1/2 cuillère à café de bicarbonate
- 1/2 cuillère à café de graines de sésame dorées
- 1 cuillère à café de coriandre moulue
- 1 cuillère à café de cumin en poudre
- 1/2 cuillère à café de piment en poudre (cayenne ou piment d'espelette)
- Sel
- Poivre
- Huile végétale pour la cuisson

#### Préparation

- Tremper les pois chiches la veille pendant 24 heures.
- Une fois le temps écoulé, égoutter les pois chiches et les faire sécher sur un torchon. Il faut absolument que ça soit sec.
- Verser les pois chiches dans le bol du mixeur et mixer le tout. Si  besoin en plusieurs fois. Il faut mixer de manière à obtenir une  préparation granuleuse et non de la purée.
- Transvaser cette préparation dans un bol.
- Toujours dans le même bol du mixeur, hacher finement le persil, la coriandre, l'oignon et l'ail.

- Ajouter les herbes aux pois chiche et mélanger.
- Vous pouvez également redonner un coup de mixeur à tout ça si vous trouvez que c'est encore trop épais.
- Puis ajouter lés épices : coriandre moulue, cumin, piment, bicarbonate, sel, poivre et mélanger.
- Bien mélanger et ajouter également les graines de sésame dorées.

- Faire chauffer votre huile.
	- À l'aide de 2 cuillères à soupe, former des falafels. Presser la préparation entre les 2 cuillères à soupe et voilà.
	- Déposer dans l'huile délicatement et retourner une fois que c'est doré.
	- Préparer du journal et du papier absorbant afin d'y déposer les falafels après cuisson.
- Servir d'une bonne salade croquante et de sauce tel que du houmous, tzatziki ou autre.

#### Accompagnement
- Salade (accompagnement)
- Tomates (accompagnement)
- Concombre (accompagnement)
- [Houmous](http://madeincooking.canalblog.com/archives/2012/12/23/25802148.html) (sauce)
- [Tzatziki ](http://madeincooking.canalblog.com/archives/2012/12/26/25802167.html)(sauce)
